function lugtoscana() {
    let ul = document.getElementById("lugtoscana");

    // https://gitlab.com/ItalianLinuxSociety/LugMap/-/raw/lugmap.linux.it/db/toscana.txt
    fetch("/lib/toscana.txt")
    .then(response => response.text())
    .then(response => {
        let lines = response.split("\n");
        for (let i = 0; i < lines.length; ++i) {
            line = lines[i].trim();
            let fields = line.split("|");
            if (fields.length < 5)
                continue;

            let lug = {
                "name": fields[1],
                "city": fields[2],
                "website": fields[3]
            };

            let li = document.createElement("li");
            let a = document.createElement("a");
            a.href = lug.website;
            a.appendChild(document.createTextNode(lug.name + " - " + lug.city));
            li.appendChild(a);

            ul.appendChild(li);

        }
    })
    .catch(err => console.error(err));

}

const MONTHNAME = [ "gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"];

function fetch_calendar() {
    const class_prefix = 'zerocalcare-'
    let ul = document.getElementById(class_prefix+"latest-events");
    fetch("/data/calendario")
    .then(response => response.json())
    .then(json => {
        for (let i = 0; i < Math.min(3, json.length) ; ++i) {
            // Do not display private events
            if (typeof json[i]['CLASS'] === 'string' && json[i]['CLASS'] == 'PRIVATE') {
                continue;
            }

            // Future improvements needed for a better backend output date in ISO format
            // Now we have to parse the string :( very very ugly
            let date = new Date(json[i]['DATETIME']);

            let m = new moment(date);
            m.locale('it');

            let eventElement = document.createElement('li');

            let titleElement = document.createElement('div');
            titleElement.appendChild(document.createTextNode(decodeURI(json[i]['SUMMARY'])));
            titleElement.classList.add(class_prefix+'title');
            eventElement.appendChild(titleElement);

            let contentEventElement = document.createElement('div');
            contentEventElement.classList.add(class_prefix+'details')
            eventElement.appendChild(contentEventElement);

            // Check if event is not confirmed
            if (typeof json[i]['STATUS'] === 'string') {
                if (json[i]['STATUS'] == 'TENTATIVE') {
                    // Make the text a bit lighter and italic
                    contentEventElement.style.fontStyle = 'italic';
                    contentEventElement.style.color = 'gray';
                    // Add note
                    let unconfirmedElement = document.createElement('div');
                    unconfirmedElement.style.fontWeight = 'bold';
                    unconfirmedElement.appendChild(document.createTextNode('⚠️ Non confermato!'));
                    contentEventElement.appendChild(unconfirmedElement);
                }
                else if (json[i]['STATUS'] == 'CANCELLED') {
                    // Strike time, date and location to remark this concept
                    dateText.style.textDecoration = 'line-through';
                    timeText.style.textDecoration = 'line-through';
                    locationText.style.textDecoration = 'line-through';
                    // Add note
                    let unconfirmedElement = document.createElement('div');
                    unconfirmedElement.style.fontWeight = 'bold';
                    unconfirmedElement.appendChild(document.createTextNode('⚠️ Cancellato!'));
                    contentEventElement.appendChild(unconfirmedElement);
                }
            }

            let dateElement = document.createElement('div');
            let dateText = document.createElement('time');
            dateText.appendChild(document.createTextNode(m.format('dddd D MMMM YYYY')));
            dateElement.appendChild(document.createTextNode('📅  '));
            dateElement.appendChild(dateText);
            contentEventElement.appendChild(dateElement);

            let timeElement = document.createElement('div');
            let timeText = document.createElement('time');
            let timeString = (json[i]['ALLDAY'] == true) ? 'Tutto il giorno' : ('ore ' + m.format('HH:mm'));
            timeText.appendChild(document.createTextNode(timeString));
            timeElement.appendChild(document.createTextNode('⏰ '));
            timeElement.appendChild(timeText);
            contentEventElement.appendChild(timeElement);

            // add if location is not empty -- default location should be selected by backend
            let locationElement = document.createElement('div');
            let locationText = document.createElement('span');
            if (json[i]['LOCATION'] !== undefined && json[i]['LOCATION'] != '') {
                let text = decodeURI(json[i]['LOCATION']);
                let textNode = document.createTextNode(text);
                if (text.match("^(http|https):\\/\\/.*$")) {
                    let anchorElement = document.createElement('a');
                    anchorElement.href = text;
                    anchorElement.appendChild(textNode);
                    locationText.appendChild(anchorElement);
                } else {
                    locationText.appendChild(textNode);
                }
            }
            else {
                locationText.appendChild(document.createTextNode('Officina Informatica'));
            }
            locationElement.appendChild(document.createTextNode('📍  '));
            locationElement.appendChild(locationText);
            contentEventElement.appendChild(locationElement);

            if (json[i]['DESCRIPTION'] !== undefined && json[i]['DESCRIPTION'] != '') {
                let descriptionElement = document.createElement('div');
                let descriptionText = document.createElement('span');
                descriptionText.appendChild(document.createTextNode(decodeURI(json[i]['DESCRIPTION'])));
                descriptionElement.appendChild(document.createTextNode('📝  '));
                descriptionElement.appendChild(descriptionText);
                descriptionElement.style.display = "none";

                // Display a clickable "..." button.
                let moreElement = document.createElement('div');
                let moreShowText = document.createElement('a');
                let moreHideText = document.createElement('a');

                moreShowText.appendChild(document.createTextNode('⬇️ Più informazioni...'));
                moreElement.appendChild(moreShowText);
                moreHideText.appendChild(document.createTextNode('⬆️ Nascondi informazioni...'));
                moreHideText.style.display = "none";
                moreElement.appendChild(moreHideText);

                moreElement.onclick = () => {
                    if (descriptionElement.style.display == "none") {
                        descriptionElement.style.display  =
                        moreHideText.style.display = "block";
                        moreShowText.style.display = "none";
                    } else {
                        descriptionElement.style.display  =
                        moreHideText.style.display = "none";
                        moreShowText.style.display = "block";
                    }
                };
                contentEventElement.appendChild(moreElement);
                contentEventElement.appendChild(descriptionElement);
            }

            ul.appendChild(eventElement);
            atLeastOne = true;
        }

        if (atLeastOne == false) {
            let warn = document.createElement('span');
            warn.textContent = 'Nessun appuntamento in programma';
            ul.appendChild(warn);
        }
    })
    .catch(err => {
        let warn = document.createElement('span');
        warn.appendChild(document.createTextNode('Non riesco a connettermi al calendario 😢'));
        console.log(class_prefix+"error: "+err);
        ul.appendChild(warn);
    });;
}

function counters() {
    fetch_calendar();

    fetch("/data/wp-latest-posts")
    .then(response => response.json())
    .then(json => {
        let ul = document.getElementById("wp-latest-posts");
        for (let i = 0; i < 3; ++i) {
            let post = json[i];
            let li = document.createElement("li");
            let a = document.createElement("a");
            a.href = post.link;
            a.appendChild(document.createTextNode(post.title.rendered));
            li.appendChild(a);
            li.appendChild(document.createElement("br"));
            let datespan = document.createElement("span");
            datespan.style.fontSize = "smaller";
            let dateobj = new Date(post.date);
            datespan.appendChild(document.createTextNode("del " + dateobj.getDate() + " " + MONTHNAME[dateobj.getMonth()] + " " + dateobj.getFullYear()));
            li.appendChild(datespan);
            ul.appendChild(li);
        }
    })
    .catch(err => console.error(err));

    fetch("/data/wp-featured-posts")
    .then(response => response.json())
    .then(json => {
        let article = document.getElementById("wp-featured-posts");

        if (json.length == 0) {
            let text = document.createTextNode("Nessun articolo in evidenza");
            article.appendChild(text);
        } else {
            let post = json[0];
            let a = document.createElement("a");
            a.href = post.link;
            a.appendChild(document.createTextNode(post.title.rendered));
            article.appendChild(a);
            let text = post.excerpt.rendered.replace(/<.*?>/g, '');
            text = text.substr(0, text.indexOf('.') + 1);
            text += ' [...]';
            let textNode = document.createTextNode(text);
            article.appendChild(document.createElement('br'));
            article.appendChild(textNode);
        }

    })
    .catch(err => console.error(err));

    fetch("/data/mw-recent-changes")
    .then(response => response.json())
    .then(json => {
        let recentchanges = json.query.recentchanges;
        let rcs = new Set();
        for (let i = 0; i < recentchanges.length; ++i) {
            rcs.add(recentchanges[i].title);
        }
        let rca = Array.from(rcs);

        let ul = document.getElementById("mw-recent-changes");
        for (let i = 0; i < rca.length; ++i) {
            let title = rca[i];
            let li = document.createElement("li");
            let a = document.createElement("a");
            a.href = "https://golem.linux.it/wiki/" + title;
            a.appendChild(document.createTextNode(title));
            li.appendChild(a);
            ul.appendChild(li);
            if (i >= 4) break;
        }
    })
    .catch(function(error){console.log(error);});

    fetch("https://blog.golem.linux.it/wp-json/wp/v2/posts")
    .then(response => {
        let articles = response.headers.get("X-WP-Total");

        let p = document.getElementById("wordpress-articles");
        p.appendChild(document.createTextNode(articles + " articoli"));
    })
    .catch(err => console.error(err));

    fetch("/data/mediawiki-pages")
    .then(response => response.json())
    .then(json => {
        let pages = json.query.statistics.articles;

        let p = document.getElementById("mediawiki-pages");
        p.appendChild(document.createTextNode(pages + " pagine"));

    })
    .catch(err => console.error(err));

    fetch("/data/gitea-repos")
    .then(response => response.json())
    .then(response => {
        let repos = response.data.length;

        let p = document.getElementById("gitea-repos");
        p.appendChild(document.createTextNode(repos + " repository"));
    })
    .catch(err => console.error(err));

}
# Home page
Proposta per rinnovo e ammodernamento homepage

Per visualizzare, utilizzare un server web, es:
```
python -m http.server 8000
```

## Deploy
Per il corretto funzionamento è necessario aggiungere un cron task periodico per aggiornare il database.
```
script/update
```

